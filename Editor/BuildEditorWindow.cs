﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
#if UNITY_2019_1_OR_NEWER
using UnityEngine.UIElements;
using UnityEditor.UIElements;
#else
using UnityEngine.Experimental.UIElements;
using UnityEngine.Experimental.UIElements.StyleSheets;
using UnityEditor.Experimental.UIElements;
#endif

namespace BuildEditor
{
    public class BuildEditorWindow : EditorWindow
    {

        [SerializeField] private BuildProfileBank profileBank;

        private SerializedObject serializedBank;
        private ReorderableList bankList;
        private int selectedBuildIndex = -1;

        [MenuItem ("BuildEditor/Open Build Window")]
        public static void Open ()
        {
            GetWindow<BuildEditorWindow> ("Build Editor");
        }

        private void OnEnable ()
        {
            if (profileBank == null)
            {
                FindProfileBankInProject (ref profileBank);
            }

            serializedBank = new SerializedObject (profileBank);
            var profileArray = serializedBank.FindProperty ("Profiles");
            CreateProfileList (ref bankList, serializedBank, profileArray);

#if UNITY_2019_1_OR_NEWER
            var root = rootVisualElement;
            var windowContainer = new IMGUIContainer (DrawWindow);
            windowContainer.style.flexGrow = 1f;
            root.Add (windowContainer);
#else
            var root = this.GetRootVisualContainer ();
            root.Add (new IMGUIContainer (DrawWindow) {
                style = {
                    flex = new Flex (1f)
                }
            });
#endif
        }

        private void OnDisable ()
        {

        }

        private void DrawWindow ()
        {
            serializedBank.Update ();
            bankList.DoLayoutList ();
            serializedBank.ApplyModifiedProperties ();

            if (GUILayout.Button ("Build Projects"))
            {
                var profiles = profileBank.BuildProfiles;
                for (var i = 0; i < profiles.Length; ++i)
                {
                    var profile = profiles[i];

                    if (profile.Active)
                    {
                        BuildPipeline.BuildPlayer (profile);
                    }
                }
            }
        }

        private void CreateProfileList (ref ReorderableList list, SerializedObject bank, SerializedProperty array)
        {
            list = new ReorderableList (bank, array);
            var elementHeight = list.elementHeight;

            list.drawHeaderCallback = r => {
                EditorGUI.LabelField (new Rect (r.x, r.y, r.width - 100f, r.height), "Profiles");
                EditorGUI.LabelField (new Rect (r.x + r.width - 100f, r.y, 100f, r.height), "Active?");
            };

            list.onSelectCallback = l => selectedBuildIndex = l.index;

            list.elementHeightCallback = i => {
                var element = array.GetArrayElementAtIndex (i);
                var height = elementHeight;

                if (i == selectedBuildIndex)
                {
                    height += EditorGUIUtility.singleLineHeight * 3f;
                    height += EditorGUI.GetPropertyHeight (element.FindPropertyRelative ("Scenes"), true);
                }

                return height;
            };

            list.drawElementCallback = (r, i, active, focused) => {
                var element = array.GetArrayElementAtIndex (i);

                EditorGUI.PropertyField (new Rect (r.x, r.y, r.width - 100f, elementHeight),
                    element.FindPropertyRelative ("Name"), GUIContent.none);
                EditorGUI.PropertyField (new Rect (r.x + r.width - 100f, r.y, 100f, elementHeight),
                    element.FindPropertyRelative ("Active"),
                    GUIContent.none);

                if (i == selectedBuildIndex)
                {
                    var yOffset = elementHeight;
                    var subElement = element.FindPropertyRelative ("Scenes");

                    for (var k = 0; k < 4; ++k)
                    {
                        var height = EditorGUI.GetPropertyHeight (subElement, true);

                        if (subElement.name != "OutputPath" && subElement.name != "Options")
                        {
                            EditorGUI.PropertyField (new Rect (r.x, r.y + yOffset, r.width, height), subElement, true);
                        } else if (subElement.name == "OutputPath")
                        {
                            EditorGUI.PropertyField (new Rect (r.x, r.y + yOffset, r.width - 100f, height), subElement, true);
                            if (GUI.Button (new Rect (r.x + r.width - 100f, r.y + yOffset, 100f, height), "Browse"))
                            {
                                var typeProp = element.FindPropertyRelative ("TargetOS");
                                var type = FetchBuildTarget (typeProp);

                                var path = EditorUtility.SaveFilePanel ("Where to build?", "", "Game", FetchFileExtension (type));
                                subElement.stringValue = path;
                            }
                        } else
                        {
                            var result = EditorGUI.EnumFlagsField (new Rect (r.x, r.y + yOffset, r.width, height), "Build Options", (BuildOptions)subElement.intValue);
                            subElement.intValue = (int)(BuildOptions)result;
                        }

                        yOffset += height;
                        subElement.NextVisible (false);
                    }
                }
            };
        }

        private void FindProfileBankInProject (ref BuildProfileBank bank)
        {
            var ids = AssetDatabase.FindAssets ("t:BuildProfileBank");
            for (var i = 0; i < ids.Length; ++i)
            {
                var path = AssetDatabase.GUIDToAssetPath (ids[i]);
                var loadedBank = AssetDatabase.LoadAssetAtPath<BuildProfileBank> (path);

                if (loadedBank != null)
                {
                    bank = loadedBank;
                    return;
                }
            }

            bank = CreateInstance<BuildProfileBank> ();
        }

        private string FetchFileExtension (BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return "apk";

                case BuildTarget.StandaloneWindows:
                    return "exe";

                case BuildTarget.StandaloneWindows64:
                    return "exe";

                case BuildTarget.StandaloneOSX:
                    return "app";

                case BuildTarget.StandaloneLinux:
                    return "x86";

                case BuildTarget.StandaloneLinux64:
                    return "x64";

                case BuildTarget.StandaloneLinuxUniversal:
                    return "x86_64";
            }

            return string.Empty;
        }

        private BuildTarget FetchBuildTarget (SerializedProperty p)
        {
            var names = p.enumNames;
            var selected = names[p.enumValueIndex];

            var enumNames = System.Enum.GetNames (typeof (BuildTarget));
            var values = System.Enum.GetValues (typeof (BuildTarget));
            for (var i = 0; i < enumNames.Length; ++i)
            {
                if (enumNames[i] == selected)
                {
                    return (BuildTarget)values.GetValue (i);
                }
            }

            return BuildTarget.NoTarget;
        }
    }
}
