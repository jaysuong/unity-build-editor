﻿using System;
using UnityEditor;

namespace BuildEditor {
    [Serializable]
    public struct BuildProfile {
        public string Name;
        public bool Active;
        public string[] Scenes;
        public string OutputPath;
        public BuildTarget TargetOS;
        public BuildOptions Options;

        public static implicit operator BuildPlayerOptions (BuildProfile profile) {
            return new BuildPlayerOptions {
                scenes = profile.Scenes,
                    locationPathName = profile.OutputPath,
                    target = profile.TargetOS,
                    options = profile.Options
            };
        }
    }
}