﻿using UnityEngine;

namespace BuildEditor {
    [CreateAssetMenu (menuName = "BuildEditor/Profile Bank")]
    public class BuildProfileBank : ScriptableObject {
#pragma warning disable 649
        [SerializeField] private BuildProfile[] Profiles = new BuildProfile[0];

#pragma warning restore 649

        public BuildProfile[] BuildProfiles => Profiles;
    }
}